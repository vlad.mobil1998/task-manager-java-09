package ru.amster.tm;

import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.model.TerminalCommand;
import ru.amster.tm.repository.CommandRepository;
import ru.amster.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String readTerminalCommand = scanner.nextLine();
            parseCmd(readTerminalCommand);
        }
    }

    private static void parseCmd(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalCmdConst.HELP:
                showHelpMsg();
                break;
            case TerminalCmdConst.ABOUT:
                showAboutMsg();
                break;
            case TerminalCmdConst.VERSION:
                showVersionMsg();
                break;
            case TerminalCmdConst.INFO:
                showInfoMsg();
                break;
            case TerminalCmdConst.ARGUMENTS:
                showArgMsg();
                break;
            case TerminalCmdConst.COMMANDS:
                showCmdMsg();
                break;
            case TerminalCmdConst.EXIT:
                exit();
                break;
            default:
                System.out.println("ERROR: Command not late");
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        for (int i = 0; i < args.length; i += 1) {
            System.out.println(" ");
            switch (args[i]) {
                case ProgramArgConst.HELP:
                    showHelpMsg();
                    break;
                case ProgramArgConst.ABOUT:
                    showAboutMsg();
                    break;
                case ProgramArgConst.VERSION:
                    showVersionMsg();
                    break;
                case ProgramArgConst.INFO:
                    showInfoMsg();
                    break;
                default:
                    System.out.println("ERROR: Argument not late");
                    break;
            }
        }
        return true;
    }

    private static void showHelpMsg() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (TerminalCommand command : commands) System.out.println(command);
    }

    private static void showArgMsg() {
        final String[] arguments = COMMAND_REPOSITORY.getARGUMENTS();
        System.out.println(Arrays.toString(arguments));
    }

    private static void showCmdMsg() {
        final String[] commands = COMMAND_REPOSITORY.getCOMMANDS();
        System.out.println(Arrays.toString(commands));
    }

    private static void showInfoMsg() {
        System.out.println("[INFO]");

        final int availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);

        NumberUtil numberUtil = new NumberUtil();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = numberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = numberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = numberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = numberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);

    }

    private static void showAboutMsg() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

    private static void showVersionMsg() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

}
