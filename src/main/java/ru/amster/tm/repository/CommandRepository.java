package ru.amster.tm.repository;

import ru.amster.tm.api.ICommandRepository;
import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final TerminalCommand HELP = new TerminalCommand(
            TerminalCmdConst.HELP, ProgramArgConst.HELP, " - Display terminal commands."
    );

    private static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalCmdConst.ABOUT, ProgramArgConst.ABOUT, " - Show developer info."
    );
    private static final TerminalCommand INFO = new TerminalCommand(
            TerminalCmdConst.INFO, ProgramArgConst.INFO, " - Display information about system."
    );
    private static final TerminalCommand VERSION = new TerminalCommand(
            TerminalCmdConst.VERSION, ProgramArgConst.VERSION, " - Show version info."
    );

    private static final TerminalCommand EXIT = new TerminalCommand(
            TerminalCmdConst.EXIT, null, " - Close application."
    );

    private static final TerminalCommand COMMAND = new TerminalCommand(
            TerminalCmdConst.COMMANDS, null, " - Display terminal command"
    );

    private static final TerminalCommand ARGUMENT = new TerminalCommand(
            TerminalCmdConst.ARGUMENTS, null, " - Display arguments program"
    );

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, EXIT, COMMAND, ARGUMENT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... value) {
        if (value == null || value.length == 0) return new String[]{};
        final String[] result = new String[value.length];
        int index = 0;
        for (int i = 0; i < value.length; i++) {
            final String name = value[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }

        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArguments(TerminalCommand... value) {
        if (value == null || value.length == 0) return new String[]{};
        final String[] result = new String[value.length];
        int index = 0;
        for (int i = 0; i < value.length; i++) {
            final String arg = value[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }

        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCOMMANDS() {
        return COMMANDS;
    }

    public String[] getARGUMENTS() {
        return ARGUMENTS;
    }
}
